package entity

import (
	"sell_data_system/servicer/config"
	"github.com/jinzhu/gorm"
	)
//User 员工用户
type User struct {
config.BaseModel
Name string `gorm:"not null" json:"name"`//员工姓名
Account string `gorm:"not null;unique" json:"account"`//账号
Password string `gorm:"not null" json:"password"`//密码
Email string `json:"email"` //邮箱
Mobil string `json:"mobil"` //电话
Address string `json:"address"` //地址
DepartmentID uint `gorm:"not null;default:0" json:"department_id"` //部门ID
CommissionType int64 `gorm:"not null;default:0" json:"commission_type"` // 提成类型：0 没有提成 1 销售额提成 2 毛利提成 3 净利润提成 4 阶梯提成
BasicPay int64 `gorm:"not null;default:0" json:"basic_pay"` // 基本工资
CommissionRate int8 `gorm:"not null;default:0" json:"commission_rate"` //提成比例
StepCode string `gorm:"index" json:"step_code"` // 阶梯提成方案code
UserPosition int8 `gorm:"not null;default:0" json:"user_position""` //员工职位 0 普通员工 1 部门领导
SuperUser uint `gorm:"not null;index" json:"super_user"` //上级领导
DepTree string `gorm:"not null" json:"dep_tree"` // TREE
}
//StepCommission 阶梯提成
type StepCommission struct {
	config.BaseModel
	StepCode string `gorm:"not null" json:"step_code"` // 阶梯提成方案， 同一方案code 相同；不同方案 ，则不同
	StepType uint8 `gorm:"not null;default:0" json:"step_type"` //阶梯类型 0 底薪 1 提成
	MinSellMoney int64 `gorm:"not null;default:0" json:"min_sell_money"` //底销售额
	MaxSellMoney int64 `gorm:"not null;default:0" json:"max_sell_money"` // 高销售额
	PayOrRate int64 `gorm:"not null;default:0" json:"pay_or_rate"` // StepType = 0 为 底薪 2 为提成比例
}
//Department 部门
type Department struct {
	config.BaseModel
	Name string `gorm:"not null;unique" json:"name"` // 部门名称
	Code string `gorm:"not null;unique" json:"code"` //部门代码
}
//SellOrder 订单
type SellOrder struct {
	config.BaseModel
	OrderNum string `gorm:"not null;unique" json:"order_num"` //订单编号
	OrderMoney int64 `gorm:"not null;default:0" json:"order_money"` //订单金额
	UserID uint `gorm:"not null;index" json:"user_id"` //用户id
	OutPay int64 `gorm:"not null;index" json:"out_pay"` //报销
	BaseMoney int64 `gorm:"not null;default:0" json:"base_money"` //订单成本
}
func (user *User) 	CreateTb(db *gorm.DB) {
	if !db.HasTable(&user) {
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&user)
	}
}
func (sc *StepCommission) CreateTb(db *gorm.DB)  {
	if !db.HasTable(&sc) {
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&sc)
	}
}
func (dep *Department) CreateTb(db *gorm.DB){
	if !db.HasTable(&dep) {
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&dep)
	}
}
func (order  *SellOrder) CreateTb(db *gorm.DB)  {
	if !db.HasTable(&order) {
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&order)
	}
}