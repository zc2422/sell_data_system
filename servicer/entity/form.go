package entity
//Login 登陆
type Login struct {
	Account string `json:"account"` // 账号
	Password string `json:"password"` //密码
	Code string `json:"code"` //验证码
}