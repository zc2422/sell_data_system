package main

import (
	"sell_data_system/servicer/server"
	"sell_data_system/servicer/database"
	"flag"
	"fmt"
)

func main() {
	port := flag.Int("port", 8080, " http listen port")
	host := flag.String("host","","http host")
	flag.Parse()
	if *port < 8080 || *port > 50000{
		fmt.Println("端口号超出8080（包扣8080）到50000（包括50000）的范围")
		return
	}
	db.InitDB(true)
	db.InitTb()

	server :=  server.Router()
    server.Run(fmt.Sprintf("%s:%d",*host,*port))
	}
