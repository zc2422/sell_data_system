package common

import (
	"strings"
	"strconv"
	"fmt"
)
// Isblank 判断字符串是否为空
func Isblank(s string) bool {
	s = strings.Trim(s," ")
	if len([]rune(s)) == 0 || s == "" {
		return  true
	}
	return  false
}
//ChangeStrToYuan 字符串元转成 float64
func ChangeStrToYuan(str string) ( float64, bool){
	if Isblank(str) { // 为空 返会false
		return 0, false
	}
	if yuan,err := strconv.ParseFloat(str,64);err != nil {

		return yuan,true
	} else {
		return 0,false
	}
}
// ChangeStrToFeng string 元转成 int64分
func ChangeStrToFeng(str string) (int64, bool) {
	if Isblank(str) { // 为空 返会false
		return 0, false
	}
	if yuan,err := strconv.ParseFloat(str,64);err == nil {
		f := yuan *100
		s := fmt.Sprintf("%.f",f)
		 if feng, err1 := strconv.ParseInt(s,10,64);err1 == nil {
		 	return feng,true
		 }
		return 0,false
	} else {
		return 0,false
	}
}
