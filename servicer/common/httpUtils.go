package common

import (
	"github.com/gin-gonic/gin"
	"net/http"
	)
//HttpResponse json响应结构体
type HttpResponse struct {
	C *gin.Context `json:"-"` // gin
	Code int64 `json:"code"` // 状态码 100 失败 200 成功 300 权限不足 500 后端错误
	Msg string `json:"msg"` // 错误提示
	Data interface{} `json:"data"` //数据

}
// DefaultHttpReponse  response响应
func (response *HttpResponse) DefaultHttpReponse() {
	response.C.JSON(http.StatusOK,&response)
}
