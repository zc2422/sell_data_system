package server

import (
	"github.com/gin-gonic/gin"
	"sell_data_system/servicer/common"
	"sell_data_system/servicer/entity"
	"fmt"
)
//Login 登陆
func Login(c *gin.Context) {
	result := &common.HttpResponse{C:c,Code:200,Msg:"登陆成功"}
	defer  result.DefaultHttpReponse()
	login := &entity.Login{}
	err := c.BindJSON(login)
	if err != nil {
		result.Code = 100
		result.Msg = "登陆参数解析错误"
		return
	}
	fmt.Printf("登陆参数：%+v",login)
	}