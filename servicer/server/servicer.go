package server

import (
	"github.com/gin-gonic/gin"
	"sell_data_system/servicer/config"
)

func Router() *gin.Engine {
	router :=  config.GetdefaultServer()
	router.GET("", func(context *gin.Context) {
		context.JSON(200,gin.H{"message":"大吉大利，今晚吃鸡"})
	})
	out := router.Group("/out")
	{
	   out.POST("/login",Login)
	}
	return router
}